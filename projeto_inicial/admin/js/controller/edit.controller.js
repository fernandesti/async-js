import { getClient, editClient } from '../service/client.service.js';

const urlParams = new URLSearchParams(window.location.search);

const startEdition = async () => {
    const client = await searchClient();
    await completeInputs(client);
}

const searchClient = async () => {
    return await getClient(urlParams.get('id'))

}

const completeInputs = ({ nome, email }) => {
    let elnome = document.querySelector('[data-nome]');
    let elemail = document.querySelector('[data-email]');
    elnome.value = nome;
    elemail.value = email;
}

startEdition();

document.querySelector('[data-form]').addEventListener('submit', event => {
    event.preventDefault();
    debugger;
    let nome = event.target.querySelector('[data-nome]').value;
    let email = event.target.querySelector('[data-email]').value;

    editClient({nome, email, id: urlParams.get('id')})
})