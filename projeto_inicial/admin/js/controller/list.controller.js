import { getClients, deleteClient } from '../service/client.service.js';

export const buildList = async () => {

    let element = document.querySelector('[data-tabela]');
    if(!element) return;
    
    const clients = await getClients();

    if(!clients) throw 'Clients not found';

    
    clients.forEach(client => {
        const trElement = document.createElement('TR');
        trElement.innerHTML =_getClientsJson(client);
        trElement.dataset.id = client.id
        element.appendChild(trElement)
    })
    
}

const _getClientsJson = ({nome, email, id}) => {
    const html = 
    `
    <td class="td" data-td>${nome}</td>
    <td>${email}</td>
    <td>
        <ul class="tabela__botoes-controle">
            <li><a href="../telas/edita_cliente.html?id=${id}" class="botao-simples botao-simples--editar">Editar</a></li>
            <li><button class="botao-simples botao-simples--excluir" type="button">Excluir</button></li>
        </ul>
    </td>
    `
    return html;
}

document.querySelector('[data-tabela]').addEventListener('click', event => {
    //event.preventDefault();
    const trElement = event.target.className === 'botao-simples botao-simples--excluir';
    if (trElement) {
        event.preventDefault();
        const trAll = event.target.closest('[data-id]');
        const id = trAll.dataset.id;
        deleteClient(id)
        .then(() => trAll.remove())
    }
})


