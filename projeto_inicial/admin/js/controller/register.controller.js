import { postClient } from '../service/client.service.js';


document.querySelector('[data-form]').addEventListener('submit', event => {
    event.preventDefault();
    debugger;
    register();
})
const register = async () => {
    const object = await builderObject();
    await validationOfObjet(object);

    if(!object.nome || !object.email) throw "Object invalid";

    await registerClient(object);
}

const registerClient = client => {
    return postClient(client);
}

const validationOfObjet = object => {
    const { nome, email} = object;

    if (!nome) {
        const alertaNome = document.querySelectorAll(".input-mensagem-erro")[0];
        alertaNome.classList.remove('input-mensagem-erro');
        alertaNome.classList.add('input-mensagem--invalido')
    };

    if (!email){
        const alertaNome = document.querySelectorAll(".input-mensagem-erro")[1];
        alertaNome.classList.remove('input-mensagem-erro');
        alertaNome.classList.add('input-mensagem--invalido')
    }
}

const builderObject = () => {
    const nome = document.querySelector('#nome').value;
    const email = document.querySelector('#email').value;

    return {nome, email};
}