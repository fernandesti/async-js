export const getClients = async () => {
     return await fetch('http://localhost:3000/profile')
            .then(data => data.json())
            .then(json => { return  json })
            .catch(err => console.log(err))  
}

export const postClient = async client => {
     return await fetch('http://localhost:3000/profile', {
          method: "POST",
          headers: {
               'Content-Type': 'application/json'
          },
          body: JSON.stringify(client)
     })
     .then(() => window.location.href = "../../telas/cadastro_concluido.html")
     .catch(() => window.location.href = "../../telas/erro.html")
}

export const deleteClient = async id => {
     return await fetch(`http://localhost:3000/profile/${id}`, {
          method: "DELETE"
     })
     .catch(erro => console.log(erro))
}

export const getClient = async id => {
     return await fetch(`http://localhost:3000/profile/${id}`)
          .then(data => data.json())
          .then(json => { return json })
          .catch(err => console.log(err))
}

export const editClient = async client => {
     return await fetch(`http://localhost:3000/profile/${client.id}`, {
          method: 'PUT',
          headers: {
               'Content-Type': 'application/json'
          },
          body: JSON.stringify({nome: client.nome, email: client.email})
     })
          .then(() => window.location.href = '../../telas/edicao_concluida.html')
          .catch(() => window.location.href = '../../telas/erro.html')
}